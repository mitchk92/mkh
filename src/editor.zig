const std = @import("std");
const te = @import("term_editor.zig");

pub const editor = struct {
    ed: te.term_editor,

    pub fn init(alloc: *std.mem.Allocator) !editor {
        return editor{
            .ed = try te.term_editor.init(alloc),
        };
    }

    pub fn run(self: *editor) !void {
        while (true) {
            self.ed.draw();
            const v = try self.ed.get_key();
            if (v) |val| {
                //self.ed.write(val);

                self.ed.print(te.print_cmd{ .text = te.text_cmd{ .text = "hello", .font = 0 } }, 1);
                self.ed.print(te.print_cmd{ .text = te.text_cmd{ .text = "world", .font = 1 } }, 2);
                self.ed.print(te.print_cmd{ .text = te.text_cmd{ .text = "test", .font = 2 } }, 3);

                switch (val) {
                    .key => |k| {
                        if (k == 'q') break;
                        var buf: [256]u8 = undefined;
                        const res = try std.fmt.bufPrint(buf[0..], "{X} ('{c}')", .{ k, k });
                        self.ed.print(te.print_cmd{ .text = te.text_cmd{
                            .text = res,
                            .font = 1,
                        } }, 5);
                    },
                    .fancy => |m| {
                        var buf: [256]u8 = undefined;
                        const res = try std.fmt.bufPrint(buf[0..], "{s}", .{@tagName(m)});

                        self.ed.print(te.print_cmd{ .text = te.text_cmd{
                            .text = res,
                            .font = 1,
                        } }, 6);
                    },
                    //else => {},
                }
            }
            //self.ed.draw();
        }
    }

    pub fn deinit(self: *editor) void {
        self.ed.deinit();
    }
};

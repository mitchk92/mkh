const std = @import("std");
const bin_section = @import("bin_sections.zig");
pub fn read_ihex(alloc: *std.mem.Allocator, file: []const u8) !bin_section.binary {
    var index: usize = 0;
    var current_section: []u8 = undefined;
    var current_addr: usize = 0;
    var line_num: usize = 0;
    var bin = bin_section.binary.init(alloc);
    while (true) {
        defer {
            line_num += 1;
        }
        const next_index = if (std.mem.indexOfAny(u8, file[index..], "\n")) |ni| ni else break;

        const line = file[index .. index + next_index];
        if (line[0] != ':') {
            return error.Invalid_format;
        }

        const byte_count = try std.fmt.parseInt(u8, line[1..3], 16);
        const addr = try std.fmt.parseInt(u16, line[3..7], 16);
        const record_type = try std.fmt.parseInt(u8, line[7..9], 16);
        const data = line[9 .. 9 + byte_count * 2];
        var byte_buffer: [256]u8 = undefined;
        var counter: usize = 0;
        while (counter < byte_count + 1) : (counter += 1) {
            byte_buffer[counter] = try std.fmt.parseInt(u8, line[9 + (counter * 2) .. 11 + (counter * 2)], 16);
        }

        const checksum = try std.fmt.parseInt(u8, line[9 + (byte_count * 2) .. 11 + (byte_count * 2)], 16);
        var count: usize = 1;
        var end_of_file: bool = false;
        var sum: u8 = 0;
        while (count <= 7 + byte_count * 2) : (count += 2) {
            const val = try std.fmt.parseInt(u8, line[count .. count + 2], 16);
            sum +%= val;
        }
        sum = ~sum +% 1;
        if (sum != checksum) {
            return error.InvalidChecksum;
        }

        switch (record_type) {
            0 => { // data
                try bin.add_data(addr + current_addr, byte_buffer[0..byte_count]);
            },
            1 => { //end of file
                break;
            },
            2 => { //extended segmetn address
                current_addr = try std.fmt.parseInt(u16, data, 16);
            },
            3 => { //start segment address
                bin.info.exec_start = @intCast(usize, try std.fmt.parseInt(u16, data, 16));
            },
            4 => { //extended linear address
                current_addr = @intCast(usize, try std.fmt.parseInt(u16, data, 16)) << 16;
            },
            5 => {
                bin.info.exec_start = @intCast(usize, try std.fmt.parseInt(u32, data, 16));
            },
            else => {
                return error.invalid_record_type;
            },
        }
        index += next_index + 1;
    }
    return bin;
}

pub fn read_srec(alloc: *std.mem.Allocator, file: []const u8) !bin_section.binary {
    var index: usize = 0;

    var req_count: ?usize = null;
    var num_records: usize = 0;
    var bin = bin_section.binary.init(alloc);
    while (true) {
        var byte_buffer = [_]u8{0} ** 256;
        const next_index = if (std.mem.indexOf(u8, file[index..], "\n")) |ni| ni else break;

        const line = file[index .. index + next_index];
        const byte_count = try std.fmt.parseInt(u8, line[2..4], 16);
        const checksum_orig = try std.fmt.parseInt(u8, line[2 + (byte_count * 2) .. 4 + (byte_count * 2)], 16);
        var count: usize = 0;
        while (count < byte_count + 1) : (count += 1) {
            byte_buffer[count] = try std.fmt.parseInt(u8, line[2 + (count * 2) .. 4 + (count * 2)], 16);
        }
        const line_data = byte_buffer[0..byte_count];
        var checksum: u8 = 0;
        for (line_data) |ld| {
            checksum +%= ld;
        }
        checksum = ~checksum;
        if (checksum != checksum_orig) {
            return error.InvalidChecksum;
        }
        if (line[0] != 'S') {
            return error.Invalid_format;
        }
        switch (line[1]) {
            '0' => {
                //std.debug.print("comment:[{s}]\n", .{@ptrCast([*:0]u8, line_data.ptr + 2)});
                //COMMent ignore
            },
            '1' => {
                //16bit
                const addr = try std.fmt.parseInt(u16, line[4..8], 16);
                const data = line[6 .. 4 + byte_count];
                num_records += 1;
                try bin.add_data(addr, line_data[3..]);
            },
            '2' => {
                const addr = try std.fmt.parseInt(u24, line[4..10], 16);
                const data = line[8 .. 4 + byte_count];
                num_records += 1;
                try bin.add_data(addr, line_data[4..]);

                //24bit
            },
            '3' => {
                const addr = try std.fmt.parseInt(u32, line[4..12], 16);
                const data = line[10 .. 4 + byte_count];
                num_records += 1;
                try bin.add_data(addr, line_data[5..]);
                //32bit
            },
            '4' => {
                return error.InvalidRecord;
            },
            '5' => {
                req_count = @intCast(usize, try std.fmt.parseInt(u16, line[4..8], 16));
            },
            '6' => {
                req_count = @intCast(usize, try std.fmt.parseInt(u24, line[4..10], 16));
            },
            '7' => {
                bin.info.exec_start = @intCast(usize, try std.fmt.parseInt(u32, line[4..12], 16));
            },
            '8' => {
                bin.info.exec_start = @intCast(usize, try std.fmt.parseInt(u32, line[4..10], 16));
            },
            '9' => {
                bin.info.exec_start = @intCast(usize, try std.fmt.parseInt(u32, line[4..8], 16));
            },
            else => {
                return error.InvalidRecord;
            },
        }
        index += next_index + 1;
    }
    if (req_count) |rc| {
        if (num_records != rc) {
            std.debug.print("{} != {}", .{ num_records, rc });
            return error.InvalidCount;
        }
    }
    return bin;
}

pub fn read_elf(alloc: *std.mem.Allocator, file: []const u8) !bin_section.binary {
    var bin = bin_section.binary.init(alloc);
    var str = std.io.fixedBufferStream(file);
    const hdr = try std.elf.Header.read(&str);
    var prog_hdr = hdr.program_header_iterator(&str);
    std.debug.print("{}\n", .{hdr});
    while (try prog_hdr.next()) |entry| {
        std.debug.print("{}\n", .{entry});
    }
    var section_hdr = hdr.section_header_iterator(&str);
    while (try section_hdr.next()) |entry| {
        std.debug.print("{}\n", .{entry});
    }

    return bin;
}

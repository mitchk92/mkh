const std = @import("std");

pub const TLV = struct {
    data: []const u8,
    tag: u32,
};

const known_tags = [_][]const u8{
    "9F01",
    "9F40",
    "81",
    "9F02",
    "9F04",
    "9F4A",
    "9F42",
    "DFDF22",
    "719F18",
    "7A9F3Y",
};

test {
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = general_purpose_allocator.deinit();
    const alloc = &general_purpose_allocator.allocator;

    for (known_tags) |tag| {}
}

pub const TLV_iterator = struct {
    data: []const u8,
    i: usize,
    pub fn init(data: []const u8) TLV_iterator {
        return TLV_iterator{
            .data = data,
            .i = 0,
        };
    }
    pub fn decode_tag(data: []const u8) !u32 {
        const data_start = try std.fmt.parseInt(u8, data[0..], 16);
    }

    pub fn next(self: *TLV_iterator) ?[]TLV {}
};

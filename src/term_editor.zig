const std = @import("std");
const ek = @import("ed_key.zig");

const pos = struct {
    x: usize,
    y: usize,
};

const key_map = struct {
    code: []const u8,
    val: ek.fancy_key,
};

const keys = [_]key_map{
    key_map{ .code = "[1~", .val = .HOME },
    key_map{ .code = "[2~", .val = .INSERT },
    key_map{ .code = "[3~", .val = .DELETE },
    key_map{ .code = "[4~", .val = .END },
    key_map{ .code = "[5~", .val = .PGUP },
    key_map{ .code = "[6~", .val = .PGDN },
    key_map{ .code = "[7~", .val = .HOME },
    key_map{ .code = "[10~", .val = .F0 },
    key_map{ .code = "[11~", .val = .F1 },
    key_map{ .code = "[12~", .val = .F2 },
    key_map{ .code = "[13~", .val = .F3 },
    key_map{ .code = "[14~", .val = .F4 },
    key_map{ .code = "[15~", .val = .F5 },
    key_map{ .code = "[17~", .val = .F6 },
    key_map{ .code = "[18~", .val = .F7 },
    key_map{ .code = "[19~", .val = .F8 },
    key_map{ .code = "[20~", .val = .F9 },
    key_map{ .code = "[21~", .val = .F10 },
    key_map{ .code = "[23~", .val = .F11 },
    key_map{ .code = "[24~", .val = .F12 },
    key_map{ .code = "[25~", .val = .F13 },
    key_map{ .code = "[26~", .val = .F14 },
    key_map{ .code = "[28~", .val = .F15 },
    key_map{ .code = "[29~", .val = .F16 },
    key_map{ .code = "[31~", .val = .F17 },
    key_map{ .code = "[32~", .val = .F18 },
    key_map{ .code = "[33~", .val = .F19 },
    key_map{ .code = "[35~", .val = .F20 },
    key_map{ .code = "[A", .val = .UP },
    key_map{ .code = "[B", .val = .DOWN },
    key_map{ .code = "[C", .val = .RIGHT },
    key_map{ .code = "[D", .val = .LEFT },
    key_map{ .code = "[F", .val = .END },
    key_map{ .code = "[G", .val = .KEYPAD_5 },
    key_map{ .code = "[H", .val = .HOME },
    key_map{ .code = "[P", .val = .F1 },
    key_map{ .code = "[Q", .val = .F2 },
    key_map{ .code = "[R", .val = .F3 },
    key_map{ .code = "[S", .val = .F4 },
};

pub const line_draw = struct {
    line: [256]u8 = undefined,
    len: usize = 0,
    pub fn line_arr(self: *line_draw) []u8 {
        return self.line[0..len];
    }
};

pub const line_buffer = struct {
    alloc: *std.mem.Allocator,
    buf: []line_draw,
    lines: usize,
};

pub const term_editor = struct {
    alloc: *std.mem.Allocator,
    width: usize,
    height: usize,
    orig: std.os.termios,
    in: std.fs.File,
    out: std.fs.File,
    input_buffer: std.fifo.LinearFifo(ek.ed_input, .Dynamic),
    line_buffer: []line_draw,

    pub fn init(alloc: *std.mem.Allocator) !term_editor {
        var ws: std.os.winsize = undefined;
        if (std.c.ioctl(1, std.os.TIOCGWINSZ, &ws) == -1) {
            return error.Invalid;
        }
        var in = std.io.getStdIn();
        var out = std.io.getStdOut();
        out.writer().print("\x1b[?1049h", .{}) catch {};

        const size = pos{ .x = ws.ws_col, .y = ws.ws_row };
        var raw = try std.os.tcgetattr(std.c.STDIN_FILENO);
        const r_orig = raw;
        raw.iflag &= @bitCast(u32, ~(@as(u32, std.os.BRKINT) | @as(u32, std.os.ICRNL) | @as(u32, std.os.INPCK) | @as(u32, std.os.ISTRIP) | @as(u32, std.os.IXON)));
        raw.oflag &= @bitCast(u32, ~(@as(u32, std.os.OPOST)));
        raw.cflag |= std.os.CS8;
        raw.lflag &= @bitCast(u32, ~(@as(u32, std.os.ECHO) | @as(u32, std.os.ICANON) | @as(u32, std.os.IEXTEN) | @as(u32, std.os.ISIG)));
        try std.os.tcsetattr(std.c.STDIN_FILENO, std.os.TCSA.FLUSH, raw);
        var lb = try alloc.alloc(line_draw, size.y);
        for (lb) |*l| l.* = line_draw{};
        return term_editor{
            .alloc = alloc,
            .width = size.x,
            .height = size.y,
            .orig = r_orig,
            .in = in,
            .out = out,
            .input_buffer = std.fifo.LinearFifo(ek.ed_input, .Dynamic).init(alloc),
            .line_buffer = lb,
        };
    }
    pub fn get_key(self: *term_editor) !?ek.ed_input {
        if (self.input_buffer.readableLength() == 0) {
            var buf: [256]u8 = [_]u8{0} ** 256;
            const len = try self.in.read(buf[0..]);

            var count: usize = 0;
            outer: while (count < len) : (count += 1) {
                if (std.ascii.isASCII(buf[count])) {
                    if (std.ascii.isCntrl(buf[count])) {
                        if (count + 1 < len) {
                            for (keys) |key| {
                                if (std.mem.indexOf(u8, buf[count..], key.code)) |val| {
                                    if (val == 1) {
                                        try self.input_buffer.writeItem(ek.ed_input{
                                            .fancy = key.val,
                                            //.key = '!',
                                        });
                                        count += key.code.len;
                                        continue :outer;
                                    }
                                }
                            }
                        }
                    } else {
                        try self.input_buffer.writeItem(ek.ed_input{
                            .key = buf[count],
                        });
                    }
                }
            }
        }
        const ret_val = self.input_buffer.readItem();

        return ret_val;
    }
    pub fn write(self: *term_editor, val: u8) void {
        self.out.writer().print("|{x}|", .{val}) catch {};
    }

    pub fn print(self: *term_editor, val: print_cmd, line: usize) void {
        switch (val) {
            .text => |t| {
                var lb = self.line_buffer[line].line[0..];
                for (lb) |*v| v.* = 0;

                const len = (std.fmt.bufPrint(lb, "{s}{s}{s}", .{ fonts[t.font], t.text, fonts[0] }) catch lb[0..0]).len;
                self.line_buffer[line].len = len;
            },
        }
    }
    pub fn draw(self: *term_editor) void {
        self.out.writer().print("\x1b[2J\x1b[H", .{}) catch {};
        const no_el: []const u8 = "";
        const el: []const u8 = "\r\n";
        for (self.line_buffer) |buf, idx| {
            const endl = if (idx == (self.line_buffer.len - 1)) no_el else el;
            if (buf.len == 0) {
                self.out.writer().print("{s}{s}{s}{s}", .{ fonts[1], "~", fonts[0], endl }) catch {};
            } else {
                self.out.writer().print("{:2}{s}{s}", .{ idx, buf.line[0..buf.len], endl }) catch {};
            }
        }
    }
    pub fn deinit(self: *term_editor) void {
        self.out.writer().print("\x1b[?1049l", .{}) catch {};

        self.input_buffer.deinit();
        std.os.tcsetattr(std.c.STDIN_FILENO, std.os.TCSA.FLUSH, self.orig) catch {};
    }
};

const fonts = [_][]const u8{
    "\x1b[0m",
    "\x1b[31m",
    "\x1b[30;47m",
};

pub const point = struct {
    x: usize,
    y: usize,
};

pub const text_cmd = struct {
    text: []const u8,
    font: u8,
    pos: ?point = null,
};

pub const print_cmd = union(enum) {
    text: text_cmd,
};

const std = @import("std");
const expect = std.testing.expect;
test {
    const text_item = struct {
        str: []const u8,
        bin: []const u8,
    };
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = general_purpose_allocator.deinit();
    const alloc = &general_purpose_allocator.allocator;

    const test_items = [_]text_item{
        text_item{ .str = "AA", .bin = "\xAA" },
        text_item{ .str = "A", .bin = "\x0A" },
        text_item{ .str = "AAB", .bin = "\x0A\xAB" },
    };

    for (test_items) |item| {
        const result = try ascii_hex_to_bin(item.str, alloc);
        defer alloc.free(result);
        expect(std.mem.eql(u8, result, item.bin));
    }
}

pub fn ascii_hex_to_bin_arr(str: []const u8, array: []u8) !usize {
    const odd = str.len % 2 == 1;
    var count: usize = 0;
    var idx: usize = 0;
    if (odd) {
        array[idx] = try std.fmt.parseInt(u8, str[0..1], 16);
        idx += 1;
        count += 1;
    }
    while (count < str.len) : (count += 2) {
        array[idx] = try std.fmt.parseInt(u8, str[count .. count + 2], 16);
        idx += 1;
    }
    return idx;
}

pub fn ascii_hex_to_bin(str: []const u8, alloc: *std.mem.Allocator) ![]u8 {
    const odd = str.len % 2 == 1;
    const len = str.len / 2 + if (odd) @as(usize, 1) else @as(usize, 0); //round up if odd
    var array = try alloc.alloc(u8, len);
    const len_r = try ascii_hex_to_bin_arr(str, array);
    return array[0..len_r];
}

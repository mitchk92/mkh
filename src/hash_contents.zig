const std = @import("std");

pub const hash_values = struct {
    md5: [std.crypto.hash.Md5.digest_length]u8,
    sha1: [std.crypto.hash.Sha1.digest_length]u8,
    sha256: [std.crypto.hash.sha2.Sha256.digest_length]u8,
    sha512: [std.crypto.hash.sha2.Sha512.digest_length]u8,
};

pub fn hash_contents(contents: []const u8) hash_values {
    var hv = hash_values{
        .md5 = undefined,
        .sha1 = undefined,
        .sha256 = undefined,
        .sha512 = undefined,
    };
    var sha1 = std.crypto.hash.Sha1.init(.{});
    sha1.update(contents);
    sha1.final(hv.sha1[0..]);

    var md5 = std.crypto.hash.Md5.init(.{});
    md5.update(contents);
    md5.final(hv.md5[0..]);

    var sha256 = std.crypto.hash.sha2.Sha256.init(.{});
    sha256.update(contents);
    sha256.final(hv.sha256[0..]);

    var sha512 = std.crypto.hash.sha2.Sha512.init(.{});
    sha512.update(contents);
    sha512.final(hv.sha512[0..]);

    return hv;
}

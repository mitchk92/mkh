const std = @import("std");

pub const binary_section = struct {
    alloc: *std.mem.Allocator,
    addr: usize,
    data: []u8,
    pub fn contains(self: *const binary_section, addr: usize) bool {
        return addr >= self.addr and addr < self.addr + self.data.len;
    }
};

pub const binary_info = struct {
    exec_start: ?usize = null,
};

pub const binary = struct {
    sections: std.ArrayList(binary_section),
    info: binary_info,
    pub fn init(alloc: *std.mem.Allocator) binary {
        return binary{
            .sections = std.ArrayList(binary_section).init(alloc),
            .info = binary_info{},
        };
    }
    pub fn deinit(self: *binary) void {
        for (self.sections.items) |*section| {
            section.alloc.free(section.data);
        }
        self.sections.deinit();
    }

    pub fn get_data(self: *binary, addr: usize, max_len: usize) ?[]u8 {
        for (self.sections.items) |*section| {
            //std.debug.print("{x} != {x}\n", .{ section.addr, addr });
            if (section.contains(addr)) {
                const offset = addr - section.addr;
                if (offset + max_len > section.data.len) {
                    return section.data[offset..];
                } else {
                    return section.data[offset .. offset + max_len];
                }
            }
        }
        return null;
    }

    pub fn amount_data(self: *binary) usize {
        var count: usize = 0;
        for (self.sections.items) |section| {
            count += section.data.len;
        }
        return count;
    }

    pub fn add_data(self: *binary, addr: usize, data: []const u8) !void {
        var post: ?*binary_section = null;
        var post_idx: usize = 0;
        var pre: ?*binary_section = null;

        for (self.sections.items) |*section, idx| {
            if (section.addr + section.data.len == addr) {
                pre = section;
            }
            if (section.addr == addr + data.len) {
                post = section;
                post_idx = idx;
            }
        }
        if (post) |_post| {
            if (pre) |_pre| {
                const pre_len: usize = _pre.data.len;
                const post_len: usize = _post.data.len;
                var ndata = try _pre.alloc.alloc(u8, data.len + pre_len + post_len);
                for (_pre.data) |val, idx| ndata[idx] = val;
                for (data) |val, idx| ndata[pre_len + idx] = val;
                for (_post.data) |val, idx| ndata[pre_len + data.len + idx] = val;
                _pre.alloc.free(_pre.data);
                _post.alloc.free(_post.data);
                _pre.data = ndata;
                _pre.addr = addr;
                _ = self.sections.orderedRemove(post_idx);
                return;
            } else {
                const len: usize = _post.data.len;
                var ndata = try _post.alloc.alloc(u8, data.len + len);
                for (data) |val, idx| ndata[idx] = val;
                for (_post.data) |val, idx| ndata[len + idx] = val;
                _post.alloc.free(_post.data);
                _post.data = ndata;
                _post.addr = addr;
                return;
            }
        } else {
            if (pre) |_pre| {
                const len: usize = _pre.data.len;
                _pre.data = try _pre.alloc.realloc(_pre.data, _pre.data.len + data.len);
                for (data) |val, idx| _pre.data[len + idx] = val;
                return;
            }
        }

        var val = try self.sections.addOne();
        val.* = binary_section{
            .alloc = self.sections.allocator,
            .addr = addr,
            .data = try self.sections.allocator.dupe(u8, data),
        };
    }
};

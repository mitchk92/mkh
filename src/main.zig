const std = @import("std");
const hash_value = @import("hash_contents.zig");
const bin_print = @import("print_bin.zig");
const read_bin = @import("read_bin.zig");
const editor = @import("editor.zig");
pub fn main() anyerror!void {
    var general_purpose_allocator = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = general_purpose_allocator.deinit();
    const alloc = &general_purpose_allocator.allocator;

    const bytes = [_]u8{ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88 };
    var bin = bin_print.bin_printer{
        .bin = bytes[0..],
        .byte_width = 16,
        .byte_header = true,
        .str = true,
    };

    const stdout = std.io.getStdOut();

    const val = @embedFile("test.hex");
    const srec = @embedFile("test.srec");
    const elf = @alignCast(8, @embedFile("test.elf"));
    //var ihex_val = try read_bin.read_ihex(alloc, val);
    //defer ihex_val.deinit();
    //var srec_val = try read_bin.read_srec(alloc, srec);
    //defer srec_val.deinit();

    var elf_val = try read_bin.read_elf(alloc, elf);
    defer elf_val.deinit();
    var count: usize = 0;
    //for (srec_val.sections.items) |item| {
    //std.debug.print("[{X}] =>|{s}|\n", .{ item.addr, std.fmt.fmtSliceHexLower(item.data) });

    //  const hex_val = ihex_val.get_data(item.addr, item.data.len);
    //  if (hex_val) |hv| {
    //     if (!std.mem.eql(u8, hv, item.data)) {
    //        std.debug.print("Not equal[{x}]|{s}|{s}\n", .{ item.addr, std.fmt.fmtSliceHexLower(hv), std.fmt.fmtSliceHexLower(item.data) });
    //       count += 1;
    //   }
    //} else {
    //    std.debug.print("No Segment\n", .{});
    //   count += 1;
    // }
    //if (count > 10) break;
    //}

    //try stdout.writer().print("NumBytes:{x}|{x}", .{ ihex_val.amount_data(), srec_val.amount_data() });
    std.log.info("\nbin:\n{}", .{bin});
    var hc = hash_value.hash_contents("hello world!\n");
    std.log.info("\nSha1:[{}]\nmd5:[{}]\nsha256:[{}]\nsha512:[{}]\n", .{
        std.fmt.fmtSliceHexLower(hc.sha1[0..]),
        std.fmt.fmtSliceHexLower(hc.md5[0..]),
        std.fmt.fmtSliceHexLower(hc.sha256[0..]),
        std.fmt.fmtSliceHexLower(hc.sha512[0..]),
    });

    var ed = try editor.editor.init(alloc);
    defer ed.deinit();
    try ed.run();
}

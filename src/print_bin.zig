const std = @import("std");

pub const bin_printer = struct {
    bin: []const u8,
    byte_width: usize,
    byte_header: bool,
    str: bool,
    pub fn format(
        self: @This(),
        comptime fmt: []const u8,
        options: std.fmt.FormatOptions,
        writer: anytype,
    ) !void {
        var count: usize = 0;

        if (self.byte_header) {
            var header_count: usize = 0;
            try std.fmt.format(writer, "{x:0>4} ", .{0});
            while (header_count < self.byte_width) : (header_count += 1) {
                try std.fmt.format(writer, "{X:>2} ", .{header_count});
            }
            try std.fmt.format(writer, "\n", .{});
        }

        while (true) {
            try std.fmt.format(writer, "{x:0>4} ", .{count});
            var byte_count: usize = 0;
            while (byte_count < self.byte_width) : (byte_count += 1) {
                if (count + byte_count >= self.bin.len) {
                    try std.fmt.format(writer, "   ", .{});
                } else {
                    try std.fmt.format(writer, "{X:0>2} ", .{self.bin[count + byte_count]});
                }
            }
            byte_count = 0;
            while (byte_count < self.byte_width) : (byte_count += 1) {
                if (count + byte_count >= self.bin.len) {
                    break;
                }
                if (std.ascii.isAlNum(self.bin[count + byte_count])) {
                    try std.fmt.format(writer, "{c}", .{self.bin[count + byte_count]});
                } else {
                    try std.fmt.format(writer, ".", .{});
                }
            }
            try std.fmt.format(writer, "\n", .{});
            count += byte_count;
            if (count >= self.bin.len) {
                break;
            }
        }
    }
};

pub fn print(binary: []const u8) void {}
